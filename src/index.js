const express = require('express');
const { sequelize, Sequelize } = require('./utils/db.js');
const dotenv = require('dotenv');
dotenv.config();
const app = express();
app.use(express.json());

// app.use(logger.log);
// app.get('/', (request, response) => {
//   response.json({ info: 'Node.js, Express, and Postgres API' });
// });
// process.on('unhandledRejection', error => {
//   // Will print "unhandledRejection err is not defined"
//   // console.log('unhandledRejection', error.message);
//   // process.exit(0);
// });
// app.use('/api', users, errorHandler);
// app.use('/api/companies', companies, errorHandler);
// app.use('/api/beverages', beverages, errorHandler);
// app.use('*', (req, res, next) =>
//   res.status(400).send('Invalid URL : Page Not Found')
// );

// app.use(logger.error);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:');
    process.kill('');
  });
const port = process.env.Port || 8060;
app.listen(port, () => console.log(`Listening on port ${port}`));
