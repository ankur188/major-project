const { sequelize, Sequelize } = require('../utils/db.js');
const uuid = require('uuid/v4'); // ES5

const Tags = sequelize.define(
  'tags',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false
    },
    back_link_count: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: { min: 0 }
    }
  },
  { freezeTableName: true, timestamps: false }
);
module.exports = {
  Tags
};
