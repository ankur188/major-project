const { sequelize, Sequelize } = require('../utils/db.js');
const uuid = require('uuid/v4'); // ES5

const Users = sequelize.define(
  'users',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    display_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    reputation: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: { min: 0 }
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: 'user'
    },
    location: {
      type: Sequelize.STRING,
      allowNull: false
    },
    created_at: {
      type: Sequelize.DATE,
      allowNull: false
    },
    about: {
      type: Sequelize.STRING,
      allowNull: true
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    }
  },
  { freezeTableName: true, timestamps: false }
);
module.exports = {
  Users
};
