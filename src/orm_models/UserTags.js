const { sequelize, Sequelize } = require('../utils/db.js');
const { Users } = require('./Users');
const { Tags } = require('./Tags');
const uuid = require('uuid/v4'); // ES5

const UserTags = sequelize.define(
  'user_tags',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    id_user: {
      type: Sequelize.UUID,
      allowNull: false
    },
    id_tag: {
      type: Sequelize.UUID,
      allowNull: false
    }
  },
  { freezeTableName: true, timestamps: false }
);

UserTags.belongsTo(Users, {
  foreignKey: { name: 'id_user' }
});

UserTags.belongsTo(Tags, {
  foreignKey: { name: 'id_tag' }
});

module.exports = {
  UserTags
};
