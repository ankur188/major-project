const { sequelize, Sequelize } = require('../utils/db.js');
const { Users } = require('./Users');
const uuid = require('uuid/v4'); // ES5

const Questions = sequelize.define(
  'questions',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    id_user: {
      type: Sequelize.UUID,
      allowNull: false
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    text: {
      type: Sequelize.STRING,
      allowNull: false
    },
    views: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: { min: 0 }
    },
    votes: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    created: {
      type: Sequelize.JSON,
      allowNull: false
    },
    modified: {
      type: Sequelize.JSON,
      allowNull: true
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    }
  },
  { freezeTableName: true, timestamps: false }
);

Questions.belongsTo(Users, {
  foreignKey: { name: 'id_user' }
});

module.exports = {
  Questions
};
