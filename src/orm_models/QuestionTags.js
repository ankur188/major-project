const { sequelize, Sequelize } = require('../utils/db.js');
const { Tags } = require('./Tags');
const { Questions } = require('./Questions');
const uuid = require('uuid/v4'); // ES5

const QuestionTags = sequelize.define(
  'question_tags',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    id_question: {
      type: Sequelize.UUID,
      allowNull: false
    },
    id_tag: {
      type: Sequelize.UUID,
      allowNull: false
    }
  },
  { freezeTableName: true, timestamps: false }
);

QuestionTags.belongsTo(Questions, {
  foreignKey: { name: 'id_question' }
});

QuestionTags.belongsTo(Tags, {
  foreignKey: { name: 'id_tag' }
});

module.exports = {
  QuestionTags
};
