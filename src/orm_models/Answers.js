const { sequelize, Sequelize } = require('../utils/db.js');
const { Questions } = require('./Questions');
const { Users } = require('./Users');
const uuid = require('uuid/v4'); // ES5

const Answers = sequelize.define(
  'answers',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    id_question: {
      type: Sequelize.UUID,
      allowNull: false
    },
    id_user: {
      type: Sequelize.UUID,
      allowNull: false
    },
    text: {
      type: Sequelize.STRING,
      allowNull: false
    },
    votes: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    created: {
      type: Sequelize.JSON,
      allowNull: false
    },
    modified: {
      type: Sequelize.JSON,
      allowNull: true
    }
  },
  { freezeTableName: true, timestamps: false }
);

Answers.belongsTo(Questions, {
  foreignKey: { name: 'id_question' }
});

Answers.belongsTo(Users, {
  foreignKey: { name: 'id_user' }
});

module.exports = {
  Answers
};
