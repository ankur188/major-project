const { sequelize, Sequelize } = require('../utils/db.js');
const { Users } = require('./Users');
const uuid = require('uuid/v4'); // ES5

const Comments = sequelize.define(
  'comments',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    // idParent: {
    //   type: Sequelize.STRING,
    //   allowNull: false
    // },
    id_user: {
      type: Sequelize.UUID,
      allowNull: false
    },
    text: {
      type: Sequelize.STRING,
      allowNull: false
    },
    created: {
      type: Sequelize.JSON,
      allowNull: false
    },
    is_parent_deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  },
  { freezeTableName: true, timestamps: false }
);
// Comments.belongsTo(Questions, {
//   foreignKey: { name: 'idParent' }
// });

// Comments.belongsTo(Answers, {
//   foreignKey: { name: 'idParent' }
// });

Comments.belongsTo(Users, {
  foreignKey: { name: 'id_user' }
});

module.exports = {
  Comments
};
