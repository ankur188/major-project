const { sequelize, Sequelize } = require('../utils/db.js');
const { Questions } = require('./Questions');
const { Comments } = require('./Comments');
const uuid = require('uuid/v4'); // ES5

const QuestionComments = sequelize.define(
  'question_comments',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    id_question: {
      type: Sequelize.UUID,
      allowNull: false
    },
    id_comments: {
      type: Sequelize.UUID,
      allowNull: false
    }
  },
  { freezeTableName: true, timestamps: false }
);

QuestionComments.belongsTo(Questions, {
  foreignKey: { name: 'idQuestion' }
});

QuestionComments.belongsTo(Comments, {
  foreignKey: { name: 'id_comment' }
});

module.exports = {
  QuestionComments
};
