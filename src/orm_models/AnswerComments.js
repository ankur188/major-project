const { sequelize, Sequelize } = require('../utils/db.js');
const { Answers } = require('./Answers');
const { Comments } = require('./Comments');
const uuid = require('uuid/v4'); // ES5

const AnswerComments = sequelize.define(
  'answer_comments',
  {
    // attributes
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: uuid()
    },
    id_answer: {
      type: Sequelize.UUID,
      allowNull: false
    },
    id_comment: {
      type: Sequelize.UUID,
      allowNull: false
    }
  },
  { freezeTableName: true, timestamps: false }
);
AnswerComments.belongsTo(Answers, {
  foreignKey: { name: 'id_answer' }
});

AnswerComments.belongsTo(Comments, {
  foreignKey: { name: 'id_comment' }
});

module.exports = {
  AnswerComments
};
