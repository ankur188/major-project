const { AnswerComments } = require('./src/orm_models/AnswerComments');
const { Answers } = require('./src/orm_models/Answers');
const { Comments } = require('./src/orm_models/Comments');
const { QuestionComments } = require('./src/orm_models/QuestionComments');
const { QuestionTags } = require('./src/orm_models/QuestionTags');
const { Questions } = require('./src/orm_models/Questions');
const { Tags } = require('./src/orm_models/Tags');
const { Users } = require('./src/orm_models/Users');
const { UserTags } = require('./src/orm_models/UserTags');

async function createCompanyTable() {
  await Users.sync({ force: true });
  await Tags.sync({ force: true });
  await Questions.sync({ force: true });
  await Answers.sync({ force: true });
  await Comments.sync({ force: true });
  await UserTags.sync({ force: true });
  await AnswerComments.sync({ force: true });
  await QuestionComments.sync({ force: true });
  await QuestionTags.sync({ force: true });
}

createCompanyTable();
